import title from './field-title.js'
import slug from './field-slug.js'
import draft from './field-draft.js'
import body from './field-body.js'
import datePublished from './field-date-published.js'
import featuredImage from './field-featured-image.js'
import gallery from './field-gallery.js'

const projects  = {
    name: 'paintings',
    label: 'Paintings',
    label_singular: 'Painting',
    folder: 'content/paintings',
    format: 'toml-frontmatter',
    create: true,
    slug: '{{title}}',
    editor: {
	preview: false
    },
    fields: [
	title,
	slug,
	draft,
	datePublished,
	featuredImage,
	gallery,
	body
    ]
}

export default projects
