import featuredImage from './field-featured-image.js'
import siteLogo from './field-site-logo.js'
import body from './field-body.js'
import galleryHome from './field-gallery-home.js'

const datas = {
	name: 'data',
	label: 'Datas',
	editor: {
		preview: false
	},
	files: [
		{
			label: 'Homepage',
			name: 'homepage',
			file: 'data/homepage.yml',
			fields: [
				featuredImage,
				siteLogo,
				galleryHome,
				body,
			]
		},
		{
			label: 'Paintings page',
			name: 'paintings',
			file: 'data/paintings.yml',
			fields: [
				featuredImage
			]
		},
		{
			label: 'Projects page',
			name: 'projects',
			file: 'data/projects.yml',
			fields: [
				featuredImage
			]
		}
	]
}

export default datas
