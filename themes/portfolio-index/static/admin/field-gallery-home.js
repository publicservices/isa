export default {
	label: 'Gallery',
	name: 'gallery',
	widget: 'list',
	required: false,
	hint: 'A list of images to present on the homepage.',
	fields: [
	 {
		label: 'Image',
		name: 'image',
		widget: 'image'
	},
	 {
		label: 'Text',
		name: 'caption',
		widget: 'string',
		required: false
	},
	 {
		label: 'Url',
		name: 'url',
		widget: 'string',
		required: false,
		hint: 'A URL is a website address like this: https://example.org'
	}
 ]
}
