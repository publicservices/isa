import collections from './collections.js'

export default {
    config: {
	// Skips config.yml.
	// By not skipping, the configs will be merged, with the js-version taking priority.
	load_config_file: false,
	display_url: window.location.origin,

	backend: {
	    name: 'gitlab',
	    branch: 'master',
	    repo: 'publicservices/isa',
	    auth_type: 'implicit',
	    app_id: 'a425196eebded9b74d500deead78995b47e44be4360c5eb9bea6c7939dad0dd4'
	},

	media_folder: 'static/media/uploads',
	public_folder: '/media/uploads',

	collections
    }
}
