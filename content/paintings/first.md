+++
title = "tiza "
slug = "tizafolder"
date_published = "2020-11-05"
image = "/media/uploads/_arrastrándose_.jpg"

[[gallery]]
image = "/media/uploads/_arrastrándose_.jpg"
caption = "Earth Erosion "
description = " *Cala Embasset, Nit de San Joan.*\n\nMallorca 2018. \\\n100x70cm\\\nPastel Rembrant in Ingres paper"

[[gallery]]
image = "/media/uploads/2.jpg"
caption = "Mediterranean Drama"
description = "Mallorca 2018\\\n40x70cm\\\nPastel sobre papel"

[[gallery]]
image = "/media/uploads/1.jpg"
caption = " North of Spain"
description = "*Picos de Europa.*\n\nLeón 2018\\\n40x60cm\\\nPastel sobre papel"

[[gallery]]
image = "/media/uploads/5.jpg"
caption = "Incendiary kid"
description = "*Incendio en Sarracó.*\n\nLeón 2018\\\n40x70cm\\\nPastel sobre papel"

[[gallery]]
image = "/media/uploads/6.jpg"
caption = "yellow reflected on crystal"
description = "León 2018\\\n100x70cm\\\nPastel sobre papel"

[[gallery]]
image = "/media/uploads/_el-circulo-mendoza_.jpg"
caption = "Mendoza Circle"
description = "Mallorca 2018\\\n40x40cm\\\nPastel sobre papel"

[[gallery]]
image = "/media/uploads/_degradación-en-el-laberinto_.jpg"
caption = "Labyrinth"
description = "León 2018\\\n100x70cm\\\nPastel sobre papel"
+++
