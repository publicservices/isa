+++
title = "Being Berliner"
slug = ""
draft = false
date_published = "2021-04-01"
image = ""

[[gallery]]
image = "/media/uploads/_ya-no-se-si-es-naturaleza-muerta-o-viva_-1-.jpg"
caption = "Alive or Dead Nature"
description = "*A flor de piel*\n\nBerlin 2020\\\n60x70cm\\\nAcrilico sobre lienzo"

[[gallery]]
image = "/media/uploads/img_1628-copy.jpg"
caption = "Directional Picture"
description = "*Vamos palante*\n\nBerlín 2020\\\n70x60cm\\\nAcrílico sobre lienzo"

[[gallery]]
image = "/media/uploads/berlin-winter.jpg"
caption = "On the way to the Berlin winter, but relaxed in Mallorca."
description = "*Esta es mi poesía de hoy, será la de todos un mañana.*\n\nBerlín 2020\\\n73x63 cm\\\nAcrílico sobre lienzo"

[[gallery]]
image = "/media/uploads/img-20200116-wa0020-1.jpg"
caption = "Árboles secos "
description = "*Mas los grises que blanco y negro*\n\nBerlín 2020\\\n73x63 cm\\\nAcrílico sobre lienzo"

[[gallery]]
image = "/media/uploads/img_9897.jpg"
caption = "Wildenbruchplatzstrasse"
description = "*Quarentine* \n\nBerlín 2020\\\n73x63 cm\\\nAcrílico sobre lienzo"
+++
